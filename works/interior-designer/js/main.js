$(function(){
    //Программное выделение пункта меню навигации, зависимое от положения прокрутки
    //$('body').scrollspy({ target: '#main-nav' });
    
    //Закрываем меню навигации после выбора пункта пользователем 
    //(для мобильной версии)
    $('#navbar ul li a').click(function() {
        $('#navbar-toggle:visible').click();
    });
    
        //Обработчик выбора пункта меню навигации.
    //Плавное пролистывание страницы до выбранного раздела.
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 900);
        event.preventDefault();
    });
    
    //Изменение положения меню навигации в зависимости от положения прокрутки
    var hiddenBtnTop = false;
    $(window).scroll(function() {
        var pointChanged = $("#services").offset().top;
        if(!hiddenBtnTop){
            if(this.scrollY > pointChanged){
                $("#btn-top").removeClass("hidden");
                hiddenBtnTop = true;
            }
        }else{
            if(this.scrollY < pointChanged){
                $("#btn-top").addClass("hidden");
                hiddenBtnTop = false;
            }
        }
    });
    
    $('.project-accordion').on('click', function () {
        $(this).find(".fa").toggleClass("fa-plus-square-o");
    });
    
    
});



