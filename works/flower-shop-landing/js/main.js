$(function () {
    //установка таймера обратного отсчета
    var naw = new Date(2016, 4 - 1, 15);
    $('#countdown').countdown({
        until: naw,
        format: 'dHM',
        layout: '<div class="col-xs-4"><div class="countdown__item"><div class="countdown__amount">{dn}</div><div class="countdown__period">{dl}</div></div></div><div class="col-xs-4"><div class="countdown__item"><div class="countdown__amount">{hn}</div><div class="countdown__period">{hl}</div></div></div><div class="col-xs-4"><div class="countdown__item"><div class="countdown__amount">{mn}</div><div class="countdown__period">{ml}</div></div></div>'
    });

    //карусель товаров
    $('.js-product-carousel').slick({
        dots: true,
        infinite: false,
        autoplay: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow: '<button type="button" class="fa fa-chevron-left slick-prev"></button>',
        nextArrow: '<button type="button" class="fa fa-chevron-right slick-next"></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
          ]
    });
    
    $(window).scroll(function() { 
        //анимация элементов раздела "как мы работаем"    
        var pointAnimated = $("#info").offset().top - 100;
        var currentPosScroll = $(this).scrollTop();
        if(currentPosScroll> pointAnimated && currentPosScroll < pointAnimated + 800){
            $(".info__item").eq(0).addClass("show animated bounceInUp");
            window.setTimeout(function(){$(".info__item").eq(1).addClass("show animated bounceInUp");}, 500);
            window.setTimeout(function(){$(".info__item").eq(2).addClass("show animated bounceInUp");}, 1000);
        }
        if(currentPosScroll < 100){
            if ( $(".info__item").hasClass('show') ){
                $(".info__item").removeClass("show animated bounceInUp");
            }
        }
    });
    
    //Загрузка яндекс карты
        ymaps.ready(init);
        var myMap, 
            myPlacemark;
        function init(){ 
            myMap = new ymaps.Map ("map", {
                center: [51.65261778, 39.19229435],
                zoom: 14
            }); 
            myPlacemark = new ymaps.Placemark([51.65261778, 39.19229435], {balloonContent: 'Цветочный магазин "Герберка"'}, {preset: 'twirl#redIcon'});
            myMap.geoObjects.add(myPlacemark);
            myMap.controls.add('zoomControl');
            myMap.controls.add('typeSelector');
        }
});